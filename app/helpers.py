from sqlalchemy.orm import Query


def zipped(items):
    items = items.all() if isinstance(items, Query) else items
    return list(zip(*items)[0]) if items else []

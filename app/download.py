# -*- coding: utf-8 -*-
import re
from datetime import date, datetime, time

from flask_script import Command
from pyquery import PyQuery as pq
import nltk
import requests

from app import app, db
from models import Post, Word
from helpers import zipped

logger = app.logger

BASE_URL = 'https://habrahabr.ru'
POSTS_LIST_PAGE_TEMPLATE = BASE_URL + '/all/page{}'
MAX_PAGE_INDEX = 100

GOOD_WORD_RE = re.compile(u'^[a-zа-я]+(-[a-zа-я]+)*$')


class PqPost(object):
    def __init__(self, d_post):
        self.pq_entry = pq(d_post)

        self.xid = int(self.pq_entry.attr('id')[5:])
        self.link = self.pq_entry('.post__title_link').attr('href')
        self.author = self.pq_entry('.post-author__link').attr('href').split('/')[-2]
        self.name = self.pq_entry('.post__title_link').text()

        time_published = self.pq_entry('.post__time_published').text()
        raw_time = time_published.rsplit(' ', 1)[-1]
        self.hour, self.minute = map(int, raw_time.split(':'))
        self.is_created_today = time_published.startswith(u'сегодня')


def _get_posts_list(page_index=1):
    url = POSTS_LIST_PAGE_TEMPLATE.format(page_index)
    response = requests.get(url)

    d_page = pq(response.text)
    d_posts = d_page('.posts .post')

    for d_post in d_posts:
        try:
            pq_post = PqPost(d_post)
        except Exception:
            logger.error('Failed to parse post from page: %s', page_index)

        yield pq_post


def get_posts_list():
    page_index = 1
    finished = False

    while not finished and page_index < MAX_PAGE_INDEX:
        for pq_post in _get_posts_list(page_index):
            if not pq_post.is_created_today:
                finished = True
                break
            yield pq_post

        page_index += 1


def get_downloaded_post_xids(day):
    return zipped(db.session.query(Post.xid).filter(Post.date >= day).all())


def get_post_text(pq_post):
    response = requests.get(pq_post.link)

    d_page = pq(response.text)
    text = d_page('.post__body_full .html_format').text()

    return text


def get_post_words(raw_text):
    tokens = nltk.word_tokenize(raw_text)
    text = nltk.Text(tokens)

    words_dict = {}
    for word in text:
        word = word.lower()

        if not GOOD_WORD_RE.match(word) or word in words_dict:
            continue

        words_dict[word] = None
        yield word


def save_post(day, pq_post, post_text, words):
    post_date = datetime.combine(day, time(pq_post.hour, pq_post.minute, 0))
    post = Post(pq_post.xid, pq_post.author, post_date, pq_post.name, post_text)
    db.session.add(post)
    db.session.flush()

    word_objects = [Word(word, post.id) for word in words]
    db.session.add_all(word_objects)


class DownloadPosts(Command):
    """Download Posts"""

    def run(self):
        today = date.today()
        downloaded_post_xids = get_downloaded_post_xids(today)

        logger.info('Started download posts (date: %s)', today)
        logger.info('Already download post count: %s', len(downloaded_post_xids))

        counter = 0
        for pq_post in get_posts_list():
            if pq_post.xid in downloaded_post_xids:
                continue

            logger.info('Find new post: %s', pq_post.xid)
            raw_text = get_post_text(pq_post)
            words = get_post_words(raw_text)
            save_post(today, pq_post, raw_text, words)

            counter += 1

        db.session.commit()

        logger.info('Total new posts: %s', counter)
        logger.info('Finished')

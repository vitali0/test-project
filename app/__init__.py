import os

from flask import Flask
from flask_sqlalchemy import SQLAlchemy

config_file = os.environ.get('APP_SETTINGS', 'config.DevelopmentConfig')

app = Flask(__name__)
app.config.from_object(config_file)

db = SQLAlchemy(app)

from app import views

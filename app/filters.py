from datetime import datetime

DATE_FMT = '%Y-%m-%d'
DATETIME_FMT = '%Y-%m-%dT%H:%M:%S'


def get_date(x, fmt):
    return datetime.strptime(x, fmt) if x is not None else None


def datetime_filter(x):
    try:
        return get_date(x, DATETIME_FMT)
    except ValueError:
        return


def date_filter(x, add_time=None):
    try:
        result = get_date(x, DATE_FMT)
        return datetime.combine(result, add_time) if result and add_time else result
    except ValueError:
        return


def text_filter(x):
    return x.lower() if x else None

import math
from datetime import datetime, time

from flask import jsonify, request
from sqlalchemy import func

from app import app, db
from filters import date_filter, datetime_filter, text_filter
from models import Post, Word
from helpers import zipped


def get_date_period_filters():
    start_date_str = request.args.get('start_date')
    start_date = datetime_filter(start_date_str) or date_filter(start_date_str, time(0, 0, 0))

    end_date_str = request.args.get('end_date')
    end_date = datetime_filter(end_date_str) or date_filter(end_date_str, time(23, 59, 59))

    return start_date, end_date


@app.route('/authors')
def get_authors():
    start_date, end_date = get_date_period_filters()

    query = db.session.query(func.distinct(Post.author))
    if start_date:
        query = query.filter(Post.date >= start_date)
    if end_date:
        query = query.filter(Post.date <= end_date)

    authors = zipped(query)

    return jsonify({'authors': authors})


@app.route('/posts')
def get_posts():
    start_date, end_date = get_date_period_filters()

    author = text_filter(request.args.get('author'))

    query = db.session.query(Post.xid, Post.date).order_by(Post.xid)
    if start_date:
        query = query.filter(Post.date >= start_date)
    if end_date:
        query = query.filter(Post.date <= end_date)
    if author:
        query = query.filter(Post.author.ilike(author))

    posts = [{'xid': p_xid, 'date': p_date} for p_xid, p_date in query.all()]

    return jsonify({'posts': posts})


@app.route('/idf')
def get_idf():
    day = request.args.get('date')
    day = date_filter(day)

    word = text_filter(request.args.get('word'))

    query = db.session.query(Word.name)
    if day:
        query = query.join(Word.post) \
            .filter(Post.date >= datetime.combine(day, time(0, 0, 0))) \
            .filter(Post.date <= datetime.combine(day, time(23, 59, 59)))

    all_words_count = query.count()
    idf = None

    if word is not None:
        current_word_count = query.filter(Word.name.ilike(word)).count()
        idf = math.log10(all_words_count / current_word_count) if current_word_count else None

    return jsonify({'idf': idf})

from sqlalchemy import Index
from sqlalchemy.orm import relationship, backref

from app import db


class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)

    xid = db.Column(db.BigInteger, nullable=False, unique=True)
    author = db.Column(db.String, nullable=False)
    date = db.Column(db.DateTime, nullable=False)

    name = db.Column(db.String, nullable=False)
    text = db.Column(db.Text, nullable=False)

    __table_args__ = (
        Index('post_author_idx', 'author'),
        Index('post_date_idx', 'date'),
        {'sqlite_autoincrement': True},
    )

    def __init__(self, xid, author, date, name, text):
        self.xid = xid
        self.author = author
        self.date = date
        self.name = name
        self.text = text

    def __repr__(self):
        return '<Post {0} {1}>'.format(self.xid, self.name)


class Word(db.Model):
    name = db.Column(db.String, nullable=False, primary_key=True)
    post_id = db.Column(db.BigInteger, db.ForeignKey('post.id'), nullable=False, primary_key=True)

    post = relationship('Post', uselist=False, backref=backref('words'))

    def __init__(self, name, post_id):
        self.name = name
        self.post_id = post_id

    def __repr__(self):
        return '<Word {0} {1}>'.format(self.name, self.post_id)

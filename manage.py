from flask_script import Manager
from flask_migrate import Migrate, MigrateCommand

from app import app, db
from app.download import DownloadPosts

migrate = Migrate(app, db)
manager = Manager(app)

manager.add_command('db', MigrateCommand)
manager.add_command('download_posts', DownloadPosts)

if __name__ == '__main__':
    manager.run()

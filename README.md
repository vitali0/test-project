Installation
============

Virtualenv creation.

```
$ mkvirtualenv test_project
$ workon test_project
```

Install requirements.
```
$ pip install -r requirements.txt
```

Configuring.

```
$ cp config_tpl.py config.py
$ vim config.py
```

Init db.

```
$ ./manage.py db upgrade
```


Running
============

Download new posts.

```
$ ./manage.py download_posts
```

Run server.

```
$ ./manage.py runserver -p 5000
```


Sample requests
===============

Get posts:

```
$ curl "http://127.0.0.1:5000/posts"
$ curl "http://127.0.0.1:5000/posts?start_date=2016-11-24"
$ curl "http://127.0.0.1:5000/posts?start_date=2016-11-24T23:10:12"
$ curl "http://127.0.0.1:5000/posts?end_date=2016-11-24T23:10:12"
$ curl "http://127.0.0.1:5000/posts?start_date=2016-11-24&end_date=2016-11-25"
$ curl "http://127.0.0.1:5000/posts?author=author"
$ curl "http://127.0.0.1:5000/posts?start_date=2016-11-24&author=author"
```

Get authors:

```
$ curl "http://127.0.0.1:5000/authors"
$ curl "http://127.0.0.1:5000/authors?start_date=2016-11-24T23:10:12"
$ curl "http://127.0.0.1:5000/authors?start_date=2016-11-24&end_date=2016-11-25"
```

Get word IDF:

```
$ curl "http://127.0.0.1:5000/idf?word=test_word"
$ curl "http://127.0.0.1:5000/idf?date=2016-11-25&word=привет"
```

"""Added Post and Word

Revision ID: f38811e133d7
Revises: 
Create Date: 2016-11-24 23:30:05.704852

"""
from alembic import op
import sqlalchemy as sa


revision = 'f38811e133d7'
down_revision = None
branch_labels = None
depends_on = None


def upgrade():
    op.create_table('post',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('xid', sa.BigInteger(), nullable=False),
        sa.Column('author', sa.String(), nullable=False),
        sa.Column('date', sa.DateTime(), nullable=False),
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('text', sa.Text(), nullable=False),

        sa.PrimaryKeyConstraint('id'),
        sa.UniqueConstraint('xid'),

        sqlite_autoincrement=True
    )

    op.create_index('post_author_idx', 'post', ['author'], unique=False)
    op.create_index('post_date_idx', 'post', ['date'], unique=False)

    op.create_table('word',
        sa.Column('name', sa.String(), nullable=False),
        sa.Column('post_id', sa.BigInteger(), nullable=False),

        sa.ForeignKeyConstraint(['post_id'], ['post.id'], ),
        sa.PrimaryKeyConstraint('name', 'post_id')
    )


def downgrade():
    op.drop_table('word')

    op.drop_index('post_date_idx', table_name='post')
    op.drop_index('post_author_idx', table_name='post')

    op.drop_table('post')
